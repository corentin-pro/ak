#include <cstdint>

#include <iostream>
#include <fstream>
#include <memory>
#include <vector>

#include "argparse.hpp"
#include "mad_decode.hpp"


struct __attribute__ ((packed)) WAVHeader
{
	char      RIFF[4] = {'R', 'I', 'F', 'F'}; // RIFF Header
	uint32_t FileSize;      // RIFF File Size
	char      WAVE[4] = {'W', 'A', 'V', 'E'}; // WAVE Header
	char      FMT[4] = {'f', 'm', 't', ' '};  // FMT header
	uint32_t FMTChunkSize;  // Size of the fmt chunk
	uint16_t AudioFormat;   // Audio format 1=PCM,6=mulaw,7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM
	uint16_t ChannelCount;  // Number of channels 1=Mono 2=Stereo
	uint32_t SampleRate;    // Sampling Frequency in Hz
	uint32_t BytesPerSec;   // bytes per second
	uint16_t BlockAlign;    // 2=16-bit mono, 4=16-bit stereo
	uint16_t BitsPerSample; // Number of bits per sample
	char      ChunkID[4] = {'d', 'a', 't', 'a'}; // "data"  string
	uint32_t ChunkSize;     // Data length
};

void decode(std::vector<char>& input_content, std::string& output_path,
				bool interlaced, bool verbose)
{
	auto input_buffer = std::make_shared<std::vector<char>>(input_content);
	MadDecoder decoder;
	if(verbose)
	{
		std::cout << "Input size : " << input_buffer->size() << std::endl;
	}
	decoder.decode(input_buffer, interlaced, verbose);
	if(verbose)
	{
		std::cout << "Input size : " << input_buffer->size() << std::endl;
	}

	WAVHeader header;
	header.FMTChunkSize = 16;
	header.AudioFormat = 1;
	header.ChannelCount = interlaced ? 2 : 1;
	header.SampleRate = 44100;
	header.BitsPerSample = 16;
	header.BlockAlign = header.BitsPerSample * header.ChannelCount / 8;
	header.BytesPerSec = header.SampleRate * header.BlockAlign;
	header.ChunkSize = decoder.output_buffers->at(0)->size() * 2; // int16 takes 2 bytes
	header.FileSize = header.ChunkSize + 44;

	if(verbose)
	{
		std::cout << "Output size : " << decoder.output_buffers->at(0)->size() << std::endl;
		std::cout << "Header size : " << sizeof(header) << std::endl;
	}
	if(interlaced)
	{
		std::ofstream output_file;
		output_file.open(output_path, std::ios::binary);
		output_file.write(reinterpret_cast<char*>(&header), sizeof(header));
		output_file.write(
			reinterpret_cast<char*>(decoder.output_buffers->at(0)->data()),
			decoder.output_buffers->at(0)->size() * 2);
		output_file.close();
	}
	else
	{
		std::ofstream output_file;
		output_file.open("left_" + output_path, std::ios::binary);
		output_file.write(reinterpret_cast<char*>(&header), sizeof(header));
		output_file.write(
			reinterpret_cast<char*>(decoder.output_buffers->at(0)->data()),
			decoder.output_buffers->at(0)->size() * 2);
		output_file.close();

		header.ChunkSize = decoder.output_buffers->at(1)->size();
		output_file.open("right_" + output_path, std::ios::binary);
		output_file.write(reinterpret_cast<char*>(&header), sizeof(header));
		output_file.write(
			reinterpret_cast<char*>(decoder.output_buffers->at(1)->data()),
			decoder.output_buffers->at(1)->size() * 2);
		output_file.close();
	}
}


int main(int argc, char** argv)
{
	argparse::ArgumentParser parser("libav");
	parser.add_argument("input_path").required().help("Path to audio file to decode");
	parser.add_argument("output_path").required().help("Path to output file");
	parser.add_argument("-v", "--verbose").help("Output additional information")
		.default_value(false).implicit_value(true);;

	try {
		parser.parse_args(argc, argv);
	}
	catch (const std::runtime_error& error) {
		std::cout << error.what() << std::endl;
		std::cout << parser;
		return 1;
	}

	bool verbose = parser["--verbose"] == true;
	std::streampos fsize = 0;
	std::ifstream input_file;
	input_file.open(parser.get<std::string>("input_path"), std::ios::binary);

	fsize = input_file.tellg();
	input_file.seekg(0, std::ios::end);
	fsize = input_file.tellg() - fsize;
	input_file.seekg(0, std::ios::beg);

	std::vector<char> input_content;
	input_content.resize(fsize);
	input_file.read(input_content.data(), fsize);
	input_file.close();

	std::string output_path = parser.get<std::string>("output_path");
	decode(input_content, output_path, true, verbose);
	decode(input_content, output_path, false, verbose);

	return 0;
}
