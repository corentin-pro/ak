#include <cstdio>

#include "mad_decode.hpp"

/*
 * This is the input callback. The purpose of this callback is to (re)fill
 * the stream buffer which is to be decoded. In this example, an entire file
 * has been mapped into memory, so we just call mad_stream_buffer() with the
 * address and length of the mapping. When this callback is called a second
 * time, we are finished decoding.
 */

enum mad_flow MadDecoder::input(void *data, struct mad_stream *stream)
{
	auto decoder = reinterpret_cast<MadDecoder*>(data);

	if (!decoder->input_buffer->size())
		return MAD_FLOW_STOP;

	mad_stream_buffer(
		stream,
		(unsigned char*)decoder->input_buffer->data(),
		decoder->input_buffer->size());

	decoder->input_buffer->clear();

	return MAD_FLOW_CONTINUE;
}

/*
 * The following utility routine performs simple rounding, clipping, and
 * scaling of MAD's high-resolution samples down to 16 bits. It does not
 * perform any dithering or noise shaping, which would be recommended to
 * obtain any exceptional audio quality. It is therefore not recommended to
 * use this routine if high-quality output is desired.
 */

inline signed int MadDecoder::scale(mad_fixed_t sample)
{
	/* round */
	sample += (1L << (MAD_F_FRACBITS - 16));

	/* clip */
	if (sample >= MAD_F_ONE)
		sample = MAD_F_ONE - 1;
	else if (sample < -MAD_F_ONE)
		sample = -MAD_F_ONE;

	/* quantize */
	return sample >> (MAD_F_FRACBITS + 1 - 16);
}

/*
 * This is the output callback function. It is called after each frame of
 * MPEG audio data has been completely decoded. The purpose of this callback
 * is to output (or play) the decoded PCM audio.
 */

enum mad_flow MadDecoder::interlaced_output(void *data, struct mad_header const *header, struct mad_pcm *pcm)
{
	auto decoder = reinterpret_cast<MadDecoder*>(data);
	unsigned int nchannels, nsamples;
	mad_fixed_t const *left_ch, *right_ch;

	/* pcm->samplerate contains the sampling frequency */

	nchannels = pcm->channels;
	nsamples  = pcm->length;
	left_ch   = pcm->samples[0];
	right_ch  = pcm->samples[1];

	signed int sample;
	while (nsamples--)
	{
		/* output sample(s) in 16-bit signed little-endian PCM */
		sample = scale(*left_ch++);
		decoder->output_buffers->at(0)->push_back(sample);

		if (nchannels == 2) {
			sample = scale(*right_ch++);
			decoder->output_buffers->at(0)->push_back(sample);
		}
	}

	return MAD_FLOW_CONTINUE;
}

enum mad_flow MadDecoder::separated_output(void *data, struct mad_header const *header, struct mad_pcm *pcm)
{
	auto decoder = reinterpret_cast<MadDecoder*>(data);
	unsigned int nchannels, nsamples;
	mad_fixed_t const *left_ch, *right_ch;

	/* pcm->samplerate contains the sampling frequency */

	nchannels = pcm->channels;
	nsamples  = pcm->length;
	left_ch   = pcm->samples[0];
	right_ch  = pcm->samples[1];

	signed int sample;
	while (nsamples--)
	{
		/* output sample(s) in 16-bit signed little-endian PCM */
		sample = scale(*left_ch++);
		decoder->output_buffers->at(0)->push_back(sample);

		if (nchannels == 2) {
			sample = scale(*right_ch++);
			decoder->output_buffers->at(1)->push_back(sample);
		}
	}

	return MAD_FLOW_CONTINUE;
}

/*
 * This is the error callback function. It is called whenever a decoding
 * error occurs. The error is indicated by stream->error; the list of
 * possible MAD_ERROR_* errors can be found in the mad.h (or stream.h)
 * header file.
 */

enum mad_flow MadDecoder::error(void *data, struct mad_stream *stream, struct mad_frame *frame)
{
	auto decoder = reinterpret_cast<MadDecoder*>(data);

	fprintf(stderr, "decoding error 0x%04x (%s) at byte offset %ld\n",
		stream->error, mad_stream_errorstr(stream),
		stream->this_frame - (unsigned char*)decoder->input_buffer->data());

	/* return MAD_FLOW_BREAK here to stop decoding (and propagate an error) */

	if(stream->error == MAD_ERROR_LOSTSYNC)
		return MAD_FLOW_CONTINUE;
	return MAD_FLOW_BREAK;
	//return MAD_FLOW_CONTINUE;
}

/*
 * This is the function called by main() above to perform all the decoding.
 * It instantiates a decoder object and configures it with the input,
 * output, and error callback functions above. A single call to
 * mad_decoder_run() continues until a callback function returns
 * MAD_FLOW_STOP (to stop decoding) or MAD_FLOW_BREAK (to stop decoding and
 * signal an error).
 */

int MadDecoder::decode(std::shared_ptr<std::vector<char>> buffer,
						  bool interlaced /*=true*/, bool verbose /*=false*/)
{
	struct mad_decoder decoder;
	int result;
	input_buffer = buffer;
	output_buffers = std::make_shared<std::vector<PCM>>();
	output_buffers->push_back(std::make_shared<std::vector<int16_t>>());
	if(!interlaced)
	{
		output_buffers->push_back(std::make_shared<std::vector<int16_t>>());
	}

	/* configure input, output, and error functions */
	mad_decoder_init(&decoder, this,
			           MadDecoder::input, 0 /* header */, 0 /* filter */,
			interlaced ? MadDecoder::interlaced_output : MadDecoder::separated_output,
			verbose ? MadDecoder::error : 0, 0 /* message */);

	/* start decoding */
	result = mad_decoder_run(&decoder, MAD_DECODER_MODE_SYNC);

	/* release the decoder */
	mad_decoder_finish(&decoder);

	return result;
}
