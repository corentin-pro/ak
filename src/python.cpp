#include <cstring>

#include <memory>
#include <vector>

#define PY_SSIZE_T_CLEAN
#include <python3.8/Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

#include "mad_decode.hpp"


namespace PyAk
{
	static PyObject* frombuffer(PyObject *self, PyObject *args, PyObject *kwargs)
	{
		PyObject* buffer = NULL;
		int interlaced = 1;
		int verbose = 0;
		MadDecoder decoder;

		const char* kwlist[] = {"buffer", "interlaced", "verbose", NULL};
		if(!PyArg_ParseTupleAndKeywords(
			args, kwargs, "S|pp:frombuffer", const_cast<char**>(kwlist), &buffer, &interlaced, &verbose))
			return NULL;

		auto input_buffer = std::make_shared<std::vector<char>>();
		input_buffer->resize(PyBytes_GET_SIZE(buffer));
		memcpy(input_buffer->data(), PyBytes_AsString(buffer), input_buffer->size());
		decoder.decode(input_buffer, interlaced==1, verbose==1);

		if(interlaced)
		{
			PyObject* python_int;
			auto result = PyList_New(decoder.output_buffers->at(0)->size());
			for(unsigned long i = 0; i < decoder.output_buffers->at(0)->size(); i++)
			{
				python_int = Py_BuildValue("i", decoder.output_buffers->at(0)->at(i));
				PyList_SetItem(result, i, python_int);
			}
			return result;
		}
		else
		{
			PyObject* python_int;
			auto channel1 = PyList_New(decoder.output_buffers->at(0)->size());
			for(unsigned long i = 0; i < decoder.output_buffers->at(0)->size(); i++)
			{
				python_int = Py_BuildValue("i", decoder.output_buffers->at(0)->at(i));
				PyList_SetItem(channel1, i, python_int);
			}
			auto channel2 = PyList_New(decoder.output_buffers->at(1)->size());
			for(unsigned long i = 0; i < decoder.output_buffers->at(1)->size(); i++)
			{
				python_int = Py_BuildValue("i", decoder.output_buffers->at(1)->at(i));
				PyList_SetItem(channel2, i, python_int);
			}
			return PyTuple_Pack(2, channel1, channel2);
		}
	}

	static PyObject* fromnumpy(PyObject *self, PyObject *args, PyObject *kwargs)
	{
		PyArrayObject *input_array;
		int interlaced = 1;
		int verbose = 0;
		MadDecoder decoder;

		const char* kwlist[] = {"buffer", "interlaced", "verbose", NULL};
		if(!PyArg_ParseTupleAndKeywords(
			args, kwargs, "O!|pp:fromnumpy", const_cast<char**>(kwlist),
			&PyArray_Type, &input_array, &interlaced, &verbose))
			return NULL;
		if(PyArray_NDIM(input_array) != 1)
		{
			PyErr_SetString(PyExc_ValueError, "array must be one-dimensional");
			return NULL;
		}
		if(PyArray_TYPE(input_array) != NPY_UINT8 && PyArray_TYPE(input_array) != NPY_INT8)
		{
			PyErr_SetString(PyExc_ValueError, "array must be of type uint8 or int8");
			return NULL;
		}

		auto input_buffer = std::make_shared<std::vector<char>>();
		input_buffer->resize(PyArray_DIMS(input_array)[0]);
		memcpy(input_buffer->data(), PyArray_DATA(input_array), input_buffer->size());
		decoder.decode(input_buffer, interlaced, verbose);

		if(interlaced)
		{
			long int dims[1];
			dims[0] = decoder.output_buffers->at(0)->size();
			int16_t* data = decoder.output_buffers->at(0)->data();
			auto swaped = new std::vector<int16_t>;
 			decoder.output_buffers->at(0)->swap(*swaped);

			auto result = (PyArrayObject*)PyArray_SimpleNewFromData(
				1,
				dims,
				NPY_INT16,
				data);
			PyArray_ENABLEFLAGS(result, NPY_ARRAY_OWNDATA);
			return PyArray_Return(result);
		}
		else
		{
			long int dims[2];
			dims[0] = decoder.output_buffers->at(0)->size();
			dims[1] = decoder.output_buffers->at(1)->size();
			auto data = new int16_t[
				decoder.output_buffers->at(0)->size() + decoder.output_buffers->at(1)->size()];
			memcpy(
				data,
				decoder.output_buffers->at(0)->data(),
				decoder.output_buffers->at(0)->size() * sizeof(int16_t));
			memcpy(
				data + decoder.output_buffers->at(0)->size(),
				decoder.output_buffers->at(1)->data(),
				decoder.output_buffers->at(1)->size() * sizeof(int16_t));
			auto result = (PyArrayObject*)PyArray_SimpleNewFromData(
				2, dims, NPY_INT16, data);
			PyArray_ENABLEFLAGS(result, NPY_ARRAY_OWNDATA);
			return PyArray_Return(result);
		}
	}
}

static PyMethodDef _coreMethods[] = {
	{"_frombuffer",  (PyCFunction)PyAk::frombuffer, METH_VARARGS | METH_KEYWORDS, "Decode given MP3 data"},
	{"_fromnumpy",  (PyCFunction)PyAk::fromnumpy, METH_VARARGS | METH_KEYWORDS, "Decode given MP3 data"},
	{NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef _coreModule = {
	PyModuleDef_HEAD_INIT,
	"core", // Module name
	NULL, // Module doc
	-1, // size of per-interpreter state of the module, or -1 if the module keeps state in global variables.
	_coreMethods
};

PyMODINIT_FUNC PyInit__core(void)
{
	import_array();
	return PyModule_Create(&_coreModule);
}

