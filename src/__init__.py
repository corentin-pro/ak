import struct
from typing import Union

import ak._core as _core
import numpy as _np


def frombuffer(buffer: bytes, interlaced: bool = True, verbose: bool = False):
    return _core._frombuffer(buffer, interlaced, verbose)


def fromnumpy(buffer: _np.ndarray, interlaced: bool = True, verbose: bool = False) -> _np.ndarray:
    return _core._fromnumpy(buffer, interlaced, verbose)


def savewav(file_path: str, pcm_data: Union[list, _np.ndarray],
            channel_count: int = 2, sample_rate: int = 44100, bits_per_sample: int = 16):
    bytes_len = len(pcm_data) * 2  # assuming int16 data
    with open(file_path, "wb") as output_file:
        header = struct.pack(
            ('c'*4)+'I'+('c'*4)+('c'*4)+'IHHIIHH'+('c'*4)+'I',
            b'R', b'I', b'F', b'F',
            bytes_len + 44,  # File size
            b'W', b'A', b'V', b'E',
            b'f', b'm', b't', b' ',
            16,  # Format data lenght (always 16 here)
            1,  # Audio format 1=PCM,6=mulaw,7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM
            channel_count,
            sample_rate,
            sample_rate * bits_per_sample * channel_count // 8,
            bits_per_sample * channel_count // 8,
            bits_per_sample,
            b'd', b'a', b't', b'a',
            bytes_len  # data size
        )
        output_file.write(header)
        if isinstance(pcm_data, _np.ndarray):
            output_file.write(pcm_data.astype('<u2').tobytes())
        else:
            for value in pcm_data:
                output_file.write(value.to_bytes(2, byteorder='little', signed=True))
