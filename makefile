CC := g++
APP := audio

SOURCE_DIR := src
OBJECT_DIR := object
OBJECT_PROD_DIR := object/prod
OBJECT_DEBUG_DIR := object/debug
INCLUDE_DIR := include

COMMON_FLAGS := -std=c++17
LINK_FLAGS := -lpthread `pkg-config --libs mad python3-embed`
COMPILE_FLAGS := -Wall `pkg-config --cflags mad python3-embed` -I/usr/lib/python3.8/site-packages/numpy/core/include

CPP_HEADERS := $(shell find $(INCLUDE_DIR) -name *.hpp)
CPP_SOURCES := $(shell find $(SOURCE_DIR) -name *.cpp)

CPP_PROD_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_PROD_DIR)/%, ${CPP_SOURCES:.cpp=.o})
CPP_DEBUG_OBJECTS := $(patsubst $(SOURCE_DIR)/%,$(OBJECT_DEBUG_DIR)/%, ${CPP_SOURCES:.cpp=.o})


# Extra options
.PHONY: all debug prod clean
.SILENT: clean

# Default build
all: prod

# Main builds
prod: COMMON_FLAGS += -O2
prod: prebuild build

debug: COMMON_FLAGS += -DDEBUG -g
debug: prebuild build_debug

prebuild:
	$(eval LINK_FLAGS = $(COMMON_FLAGS) $(LINK_FLAGS))
	$(eval COMPILE_FLAGS = $(COMMON_FLAGS) $(COMPILE_FLAGS) -I$(INCLUDE_DIR))

# Final link
build: $(CPP_PROD_OBJECTS)
	@echo -e ""
	@echo -e "\033[1m\033[92m### Linking application\033[0m"
	$(CC) $(CPP_PROD_OBJECTS) -o $(APP) $(LINK_FLAGS)
	@echo -e ""
	@echo -e ""

build_debug: $(CPP_DEBUG_OBJECTS)
	@echo -e ""
	@echo -e "\033[1m\033[92m### Linking application in DEBUG mode\033[0m"
	$(CC) $(CPP_DEBUG_OBJECTS) -o $(APP)_debug $(LINK_FLAGS)
	@echo -e ""

# Compile
$(OBJECT_PROD_DIR)/%.o: $(SOURCE_DIR)/%.cpp $(CPP_HEADERS)
	@echo -e "\033[92m### Compiling $<\033[0m"
	@mkdir -p $(dir $@)
	$(CC) $(COMPILE_FLAGS) $< -c -o $@

$(OBJECT_DEBUG_DIR)/%.o: $(SOURCE_DIR)/%.cpp $(CPP_HEADERS)
	@echo -e "\033[92m### Compiling $< in DEBUG mode\033[0m"
	@mkdir -p $(dir $@)
	$(CC) $(COMPILE_FLAGS) $< -c -o $@


# Mandatory clean
clean:
	@rm -rf $(OBJECT_DIR)
	@rm -f $(APP)
	@rm -f $(APP)_debug
	@echo -e "\033[1m\033[93m### Project cleaned $<\033[0m"
	@echo -e ""
