import glob
import os
import subprocess
import sys

from distutils.core import setup, Extension
from distutils.sysconfig import get_python_lib

includes = [include[2:] for include in subprocess.check_output(
    f'pkg-config --cflags mad python-{sys.version_info.major}.{sys.version_info.minor}-embed',
    shell=True).decode().strip().split(' ')]
libs = [library[2:] for library in subprocess.check_output(
    f'pkg-config --libs mad python-{sys.version_info.major}.{sys.version_info.minor}-embed',
    shell=True).decode().strip().split(' ')]
sources = glob.glob(os.path.join('src', '**', '*.c*'), recursive=True)

sources.remove(os.path.join('src', 'main.cpp'))
includes.append('include')
includes.append(os.path.join(get_python_lib(), 'numpy', 'core', 'include'))

module1 = Extension('ak._core', sources=sources, include_dirs=includes, libraries=libs)

setup(name='ak',
      version='1.0',
      description='Decode MP3 data',
      packages=['ak'],
      package_dir={'ak': 'src'},
      ext_modules=[module1])
