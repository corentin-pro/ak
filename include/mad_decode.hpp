#pragma once

#include <memory>
#include <vector>

#include <mad.h>


class MadDecoder
{
	using PCM = std::shared_ptr<std::vector<int16_t>>;
public:
	static enum mad_flow input(void *data, struct mad_stream *stream);
	static inline signed int scale(mad_fixed_t sample);
	static enum mad_flow interlaced_output(
		void *data, struct mad_header const *header, struct mad_pcm *pcm);
	static enum mad_flow separated_output(
		void *data, struct mad_header const *header, struct mad_pcm *pcm);
	static enum mad_flow error(void *data, struct mad_stream *stream, struct mad_frame *frame);

	std::shared_ptr<std::vector<char>> input_buffer;
	std::shared_ptr<std::vector<PCM>> output_buffers;
	int decode(
		std::shared_ptr<std::vector<char>>,
		bool interlaced=true, bool verbose=false);
};
