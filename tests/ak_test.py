from argparse import ArgumentParser
import os
import time

import ak
import numpy as np


def main():
    parser = ArgumentParser()
    parser.add_argument('input_file', help='Audio file to read')
    parser.add_argument('--output-file', default='mp3_test.wav', help='Output file')
    parser.add_argument('--benchmark', type=int, help='Loop range for benchmarking')
    arguments = parser.parse_args()

    if not os.path.exists(arguments.input_file):
        print(f'Error : could not find "{arguments.input_file}"')
        exit(1)
    with open(arguments.input_file, "rb") as input_file:
        input_content = input_file.read()

    pcm = ak.frombuffer(input_content)
    ak.savewav(arguments.output_file, pcm)

    pcm = ak.frombuffer(input_content, interlaced=False)
    ak.savewav('left_' + arguments.output_file, pcm[0], channel_count=1)
    ak.savewav('right_' + arguments.output_file, pcm[1], channel_count=1)

    numpy_buffer = np.frombuffer(input_content, dtype=np.uint8)
    pcm = ak.fromnumpy(numpy_buffer, interlaced=True)
    ak.savewav('numpy_' + arguments.output_file, pcm)

    pcm = ak.fromnumpy(numpy_buffer, interlaced=False)
    ak.savewav('numpy_left_' + arguments.output_file, pcm[0], channel_count=1)
    ak.savewav('numpy_right_' + arguments.output_file, pcm[1], channel_count=1)

    if arguments.benchmark:
        start_time = time.time()
        for _ in range(arguments.benchmark):
            pcm = ak.frombuffer(input_content, interlaced=True)
        print(f'frombuffer interlaced : {arguments.benchmark / (time.time() - start_time):.03f}/s')
        start_time = time.time()
        for _ in range(arguments.benchmark):
            pcm = ak.frombuffer(input_content, interlaced=False)
        print(f'frombuffer separated : {arguments.benchmark / (time.time() - start_time):.03f}/s')
        numpy_buffer = np.frombuffer(input_content, dtype=np.uint8)
        start_time = time.time()
        for _ in range(arguments.benchmark):
            pcm = ak.fromnumpy(numpy_buffer, interlaced=True)
        print(f'fromnumpy interlaced : {arguments.benchmark / (time.time() - start_time):.03f}/s')
        start_time = time.time()
        for _ in range(arguments.benchmark):
            pcm = ak.fromnumpy(numpy_buffer, interlaced=False)
        print(f'fromnumpy separated : {arguments.benchmark / (time.time() - start_time):.03f}/s')


if __name__ == '__main__':
    main()
